" smpl - a vim colorscheme with a configurable color palette
" Maintainer: Gunnar Ludwig
" Version:    0.2.0
" License:    ...

" Standard Colorscheme Boilerplate {{{

highlight clear
if exists('syntax_on')
  syntax reset
endif
let g:colors_name = 'smpl'

" }}}

" Utility Functions {{{

" prints a warning message
function! s:Warn(msg)
  echohl WarningMsg
  echomsg 'smpl: ' . a:msg
  echohl NONE
endfunction

" ensures the given dictionary only contains rgb hex colors
function! s:CheckPalette(palette)
  for color in values(a:palette)
    if color !~# '^#\x\{6}$'
      call s:Warn('invalid palette color: ' . color)
      return 0
    endif
  endfor
  return 1
endfunction

" Sets the text color, background color, and attributes for the given
" highlight group, in both terminal and gui vim. The values of a:hlgroup and
" a:attr are directly inserted into a highlight command. Valid values for
" a:fg and a:bg include the empty string (indicating NONE) and the first
" eight items in s:color_indices.
function! s:Style(hlgroup, fg, bg, attr)
  " get terminal color index
  let l:fg_idx = index(s:color_indices, a:fg)
  let l:bg_idx = index(s:color_indices, a:bg)

  let l:ctermfg = l:fg_idx == -1 ? 'NONE' : l:fg_idx
  let l:ctermbg = l:bg_idx == -1 ? 'NONE' : l:bg_idx
  let l:guifg   = a:fg     == '' ? 'NONE' : a:fg
  let l:guibg   = a:bg     == '' ? 'NONE' : a:bg
  let l:attr    = a:attr   == '' ? 'NONE' : a:attr

  " use bright colors with the bold attr
  if a:attr =~# 'bold' && (0 <= l:fg_idx && l:fg_idx < 8)
    let l:guifg = s:color_indices[l:fg_idx + 8]
  endif

  execute 'highlight ' . a:hlgroup . ' ctermfg=' . l:ctermfg . ' ctermbg=' .
    \ l:ctermbg . ' cterm=' . l:attr . ' guifg=' . l:guifg . ' guibg=' .
    \ l:guibg . ' gui=' . l:attr
endfunction
" }}}

" Set Color Palette {{{

" \'visual': '#81A3CF',
" Default gui colors if background is *light* and no custom palette is used.
let s:default_light = {
  \'text':              '#ffffff',
  \'yamlstring':        '#000000',
  \'visual':            '#005fd7',
  \'black':             '#000000',
  \'white':             '#ffffff',
  \'linenr':            '#a8a8a8',
  \'grey':              '#6c6c6c',
  \'light_grey':        '#eeeeee', 
  \'menu_grey':         '#404040',
  \'search_grey':       '#222222',
  \'string_grey':       '#808080',
  \'context_grey':      '#404040',
  \'red':               '#ff0000',
  \'bright_red':        '#C95F5F',
  \'lightblue':         '#0000af', 
  \'search_gold':       '#ffd700',
  \'green':             '#008700',
  \'bright_green':      '#8ae234',
  \}

" Default gui colors if background is *dark* and no custom palette is used.
let s:default_dark = {
  \'text':              '#000000',
  \'yamlstring':        '#ffffff',
  \'visual':            '#81A3CF',
  \'black':             '#000000',
  \'white':             '#ffffff',
  \'linenr':            '#6c6c6c',
  \'grey':              '#6c6c6c',
  \'light_grey':        '#303030', 
  \'menu_grey':         '#a8a8a8',
  \'search_grey':       '#222222',
  \'string_grey':       '#bcbcbc',
  \'context_grey':      '#404040',
  \'red':               '#ff0000',
  \'bright_red':        '#C95F5F',
  \'lightblue':         '#81A3CF', 
  \'search_gold':       '#ffd700',
  \'green':             '#008700',
  \'bright_green':      '#8ae234',
  \}

" choose default colors based on background
if &background == 'light'
  let s:palette = s:default_light
else
  let s:palette = s:default_dark
endif

" override default colors with custom palette
if exists('g:smpl_palette')
  if s:CheckPalette(g:smpl_palette)
    call extend(s:palette, g:smpl_palette)
  else
    call s:Warn('using default palette instead')
  endif
endif

" Set some convenience variables so that, e.g. s:palette.red can be referred
" to as s:red.
call extend(s:, s:palette)

" used to look up the corresponding terminal color index for a color
let s:color_indices = [ s:black, s:red, s:white, s:grey, s:light_grey, s:menu_grey, s:search_grey, s:string_grey, s:context_grey, s:bright_red, s:lightblue, s:search_gold, s:green, s:bright_green ]

" }}}


""           HIGHLIGHT GROUP   TEXT       BACKGROUND ATTRIBUTES
call s:Style('Boolean', '', '', '')
call s:Style('Character', '', '', '')
call s:Style('ColorColumn', s:bright_red, s:light_grey, 'BOLD')
call s:Style('Comment', s:grey, '', '')
call s:Style('Conditional', '', '', '')
call s:Style('Constant', '', '', '')
call s:Style('CursorColumn', '', '', '')
call s:Style('Cursor', '', '', '')
call s:Style('CursorLineNr', s:linenr, '', '')
call s:Style('CursorLine', '', s:light_grey, 'BOLD')
call s:Style('Define', '', '', '')
call s:Style('Directory', '', '', '')
call s:Style('ErrorMsg', '', '', '')
call s:Style('Float', s:bright_red, '', 'BOLD')
call s:Style('Folded', s:grey, '', '')
call s:Style('Function', '', '', 'BOLD')
call s:Style('Identifier', '', '', '')
call s:Style('IncSearch', '', '', '')
call s:Style('Keyword', '', '', '')
call s:Style('Label', '', '', '')
call s:Style('LineNr', s:linenr, '', '')
call s:Style('MatchParen', s:bright_red, '', 'BOLD')
call s:Style('NonText', s:grey, '', '')
call s:Style('Normal', '', 'BOLD', '')
call s:Style('Number', s:bright_red, '', 'BOLD')
call s:Style('Operator', '', '', '')
call s:Style('Conceal', s:lightblue, '', '')
call s:Style('FoldColumn', s:lightblue, '', '')
call s:Style('Exception', s:grey, s:red, '')
call s:Style('Macro', s:lightblue, '', '')
call s:Style('PreCondit', s:lightblue, '', '')
call s:Style('Structure', s:lightblue, '', '')
call s:Style('Typedef', s:lightblue, '', 'BOLD')
call s:Style('Delimiter', '', '', '')
call s:Style('SpecialComment', s:grey, '', '')
call s:Style('Debug', s:lightblue, '', '')
call s:Style('Error', '', s:bright_red, '')
call s:Style('Global', s:lightblue, '', '')

call s:Style('PmenuSel', '', '', 'REVERSE')
call s:Style('Pmenu', s:lightblue, s:light_grey, '')
call s:Style('PmenuThumb', '', '', '')
call s:Style('PmenuSbar', '', '', '')

call s:Style('PreProc', '', '', '')
call s:Style('Repeat', '', '', '')
call s:Style('Search', s:search_grey, s:search_gold, '')
call s:Style('SignColumn', '', '', '')
call s:Style('Special', '', '', '')
call s:Style('SpecialChar', '', '', '')
call s:Style('SpecialKey', '', '', '')
call s:Style('Statement', '', '', '')
call s:Style('StatusLine', '', '', '')
call s:Style('StatusLineNC', '', s:grey, '')
call s:Style('StorageClass', '', '', '')
call s:Style('String', s:string_grey, '', '')
call s:Style('Tag', '', '', '')
call s:Style('Title', '', '', '')
call s:Style('Todo', s:bright_red, 'BOLD', '')
call s:Style('Type', s:lightblue, '', '')
call s:Style('Underlined', '', '', '')
call s:Style('VertSplit', '', '', '')
call s:Style('Visual', s:text, s:visual, '')
call s:Style('StatusLineTerm', '', '', '')
call s:Style('StatusLineTermNC', '', '', '')
call s:Style('ModeMsg', '', '', '')
call s:Style('MoreMsg', '', '', '')
call s:Style('WarningMsg', '', '', '')
call s:Style('Question', '', '', '')
call s:Style('WildMenu', '', '', '')
call s:Style('TabLine', s:white, s:black, '')
call s:Style('TabLineFill', s:white, s:black, '')
call s:Style('TabLineSel', s:white, s:black, '')
call s:Style('BufTabLineCurrent', s:white, s:black, '')
call s:Style('BufTabLineActive', s:white, s:black, '')
call s:Style('BufTabLineHidden', s:white, s:black, '')
call s:Style('BufTabLineFill', s:white, s:black, '')
call s:Style('Include', '', '', '')

call s:Style('CocErrorFloat', s:white, s:black, '')
call s:Style('TSVariable', '', '', '')
call s:Style('TSPunctBracket', '', '', '')
call s:Style('TSString', s:string_grey, '', '')
call s:Style('TSConstBuiltin', s:lightblue, '', '')
call s:Style('TSConstant', '', '', '')
call s:Style('TSConditional', s:lightblue, '', '')
call s:Style('TSKeywordFunction', s:lightblue, '', '')
call s:Style('TSNumber', s:bright_red, '', '')
call s:Style('TSBoolean', '', '', '')
call s:Style('TSParameter', '', '', '')
call s:Style('TSOperator', '', '', '')
call s:Style('TSType', '', '', '')
call s:Style('TSProperty', '', '', '')
call s:Style('TSMethod', '', '', '')
call s:Style('TSPunctDelimiter', '', '', '')
call s:Style('TSTypeBuiltin', '', '', '')
call s:Style('TSField', s:lightblue, '', '')
call s:Style('TSPunctDelimiter', '', '', '')
call s:Style('TSKeyword', s:lightblue, '', '')

call s:Style('ContextVt', s:context_grey, '', 'BOLD')

call s:Style('FocusedSymbol',  '',   '',      'reverse,bold')
call s:Style('NormalFloat',    '',   s:search_grey,  '')
call s:Style('GHTextViewDark', '',   s:search_grey,  '')
call s:Style('GHListDark',     '',   s:search_grey,  '')
call s:Style('GHListHl',       s:text,   s:visual,  '')
call s:Style('GHGbDark',       '',   s:black,  '')

call s:Style('goBuiltins', s:lightblue, '', '')
call s:Style('goComment', s:grey, '', '')
call s:Style('goComplexes', '', '', '')
call s:Style('goTSKeywordFunction', s:lightblue, '', 'BOLD')
call s:Style('goTSBoolean', '', '', '')
call s:Style('goTSInclude', s:lightblue, '', '')
call s:Style('goTSParameter', '', '', '')
call s:Style('goTSVariable', '', '', '')
call s:Style('goTSOperator', '', '', '')
call s:Style('goTSProperty', '', '', '')
call s:Style('goTSTypeBuiltin', '', '', '')
call s:Style('goTSMethod', '', '', '')
call s:Style('goConditional', '', '', '')
call s:Style('goTSConditional', '', '', '')
call s:Style('goTSConstBuiltin', '', '', '')
call s:Style('goTSKeyword', s:lightblue, '', '')
call s:Style('goConst', s:lightblue, '', '')
call s:Style('goTSConstant', '', '', '')
call s:Style('goDeclaration', s:lightblue, '', '')
call s:Style('goDirective', '', '', '')
call s:Style('goExtraType', '', '', '')
call s:Style('goFloats', s:lightblue, '', '')
call s:Style('goTSNumber', s:bright_red, '', '')
call s:Style('goFormatSpecifier', s:lightblue, '', '')
call s:Style('goFunction', s:white, '', 'BOLD')
call s:Style('goTSFunction', s:white, '', 'BOLD')
call s:Style('goLabel', '', '', '')
call s:Style('goMethod', '', '', '')
call s:Style('goTSPunctBracket', '', '', '')
call s:Style('goTSPunctDelimiter', '', '', '')
call s:Style('goRepeat', s:lightblue, '', '')
call s:Style('goSignedInts', s:lightblue, '', '')
call s:Style('goStatement', s:white, '', '')
call s:Style('goString', s:string_grey, '', '')
call s:Style('goTSString', s:string_grey, '', '')
call s:Style('goStruct', s:grey, '', '')
call s:Style('goStructDef', s:lightblue, '', '')
call s:Style('goType', '', '', '')
call s:Style('goTSType', '', '', '')
call s:Style('goVar', s:lightblue, '', '')
call s:Style('goUnsignedInts', s:lightblue, '', '')
call s:Style('goCoverageNormalText', '', '', '')
call s:Style('goCoverageUncover', s:bright_red, '', '')
call s:Style('goCoverageCovered', s:green, '', '')
call s:Style('godocConstBlock', s:lightblue, '', '')
call s:Style('godocConst', '', '', '')
call s:Style('godocFunction', s:lightblue, '', '')
call s:Style('godocMethod', '', '', '')
call s:Style('godocMethodRec', s:lightblue, '', '')
call s:Style('godocTitle', s:lightblue, '', '')
call s:Style('godocType', s:lightblue, '', '')
call s:Style('godocVarBlock', s:lightblue, '', '')
call s:Style('godocVar', s:lightblue, '', '')
call s:Style('goConstants', s:lightblue, '', '')
call s:Style('goTodo', s:bright_red, '', '')
call s:Style('goDeclType', s:lightblue, '', '')

call s:Style('markdownH1', s:lightblue, '', '')
call s:Style('markdownH2', s:lightblue, '', '')
call s:Style('markdownH3', s:lightblue, '', '')
call s:Style('markdownH4', s:lightblue, '', '')
call s:Style('markdownH5', s:lightblue, '', '')
call s:Style('markdownH6', s:lightblue, '', '')
call s:Style('markdownListMarker', s:lightblue, '', '')
call s:Style('markdownHeadingRule', s:lightblue, '', '')
call s:Style('markdownBlockquote', s:lightblue, '', '')
call s:Style('markdownCodeBlock', s:lightblue, '', '')
call s:Style('markdownCode', s:lightblue, '', '')
call s:Style('markdownLink', s:bright_red, '', '')
call s:Style('markdownUrl', s:bright_red, '', '')
call s:Style('markdownLinkText', s:bright_red, '', '')
call s:Style('markdownLinkTextDelimiter', s:bright_red, '', '')
call s:Style('markdownLinkDelimiter', s:bright_red, '', '')
call s:Style('markdownCodeDelimiter', s:lightblue, '', '')
call s:Style('mkdCode', s:bright_red, '', '')
call s:Style('mkdLink', s:bright_red, '', '')
call s:Style('mkdURL', s:bright_red, '', '')
call s:Style('mkdString', s:grey, '', '')
call s:Style('mkdBlockQuote', s:grey, '', '')
call s:Style('mkdLinkTitle', s:bright_red, '', '')
call s:Style('mkdDelimiter', s:bright_red, '', '')
call s:Style('mkdRule', s:bright_red, '', '')

call s:Style('javascriptFunction', '', '', '')
call s:Style('javaScriptIdentifier', '', '', '')
call s:Style('javaScriptRepeat', '', '', '')
call s:Style('javascriptConditional', '', '', '')
call s:Style('javascriptStatement', '', '', '')
call s:Style('javaScriptBraces', '', '', '')
call s:Style('javaScriptParens', '', '', '')
call s:Style('javaScriptBoolean', '', '', '')
call s:Style('javaScriptNumber', '', '', '')
call s:Style('javaScriptMember', '', '', '')
call s:Style('javaScriptReserved', '', '', '')
call s:Style('javascriptNull', '', '', '')
call s:Style('javascriptGlobal', '', '', '')
call s:Style('javaScriptMessage', '', '', '')

call s:Style('jsonKeyword', '', '', '')
call s:Style('jsonString', s:grey, '', '')
call s:Style('jsonQuote', '', '', '')
call s:Style('jsonNoise', '', '', '')
call s:Style('jsonKeywordMatch', '', '', '')
call s:Style('jsonBraces', '', '', '')
call s:Style('jsonNumber', '', '', '')
call s:Style('jsonNull', '', '', '')
call s:Style('jsonBoolean', '', '', '')
call s:Style('jsonCommentError', '', '', '')

call s:Style('haskellBottom', '', '', '')
call s:Style('haskellTH', '', '', '')
call s:Style('haskellIdentifier', '', '', '')
call s:Style('haskellForeignKeywords', '', '', '')
call s:Style('haskellKeyword', '', '', '')
call s:Style('haskellDefault', '', '', '')
call s:Style('haskellConditional', '', '', '')
call s:Style('haskellNumber', '', '', '')
call s:Style('haskellFloat', '', '', '')
call s:Style('haskellSeparator', '', '', '')
call s:Style('haskellDelimiter', '', '', '')
call s:Style('haskellInfix', '', '', '')
call s:Style('haskellOperators', '', '', '')
call s:Style('haskellQuote', '', '', '')
call s:Style('haskellShebang', '', '', '')
call s:Style('haskellLineComment', '', '', '')
call s:Style('haskellBlockComment', '', '', '')
call s:Style('haskellPragma', '', '', '')
call s:Style('haskellLiquid', '', '', '')
call s:Style('haskellString', s:grey, '', '')
call s:Style('haskellChar', '', '', '')
call s:Style('haskellBacktick', '', '', '')
call s:Style('haskellQuasiQuoted', '', '', '')
call s:Style('haskellTodo', s:bright_red, '', '')
call s:Style('haskellPreProc', '', '', '')
call s:Style('haskellAssocType', '', '', '')
call s:Style('haskellQuotedType', '', '', '')
call s:Style('haskellType', '', '', 'BOLD')
call s:Style('haskellImportKeywords', '', '', '')

call s:Style('haskellDeclKeyword', '', '', '')
call s:Style('HaskellDerive', '', '', '')
call s:Style('haskellDecl', '', '', '')
call s:Style('haskellWhere', '', '', '')
call s:Style('haskellLet', '', '', '')

call s:Style('haskellStatement', '', '', '')

call s:Style('cabalIdentifier', '', '', '')
call s:Style('cabalLineComment', '', '', '')
call s:Style('cabalOperator', '', '', '')
call s:Style('cabalColon', '', '', '')
call s:Style('cabalNumber', '', '', '')
call s:Style('cabalSection', '', '', '')
call s:Style('cabalDelimiter', '', '', '')
call s:Style('cabalBool', '', '', '')
call s:Style('cabalCompilerFlag', '', '', '')
call s:Style('cabalConditional', '', '', '')
call s:Style('cabalDocBulletPoint', '', '', '')
call s:Style('cabalDocHeadline', '', '', '')
call s:Style('cabalDocNewline', '', '', '')
call s:Style('cabalDocCode', '', '', '')

call s:Style('dockerfileKeywords', s:lightblue, '', '')
call s:Style('dockerfileEnv', s:lightblue, '', '')
call s:Style('dockerfileString', s:grey, '', '')
call s:Style('dockerfileString1', s:grey, '', '')
call s:Style('dockerfileComment', s:grey, '', '')
call s:Style('dockerfileEmail', s:grey, '', '')
call s:Style('dockerfileUrl', s:grey, '', '')
call s:Style('dockerfileTodo', s:grey, '', '')

call s:Style('dockerfileKeyword', s:lightblue, '', '')
call s:Style('shDerefVar', s:lightblue, '', '')
call s:Style('shOperator', s:lightblue, '', '')
call s:Style('shOption', s:lightblue, '', '')
call s:Style('shLine', s:lightblue, '', '')
call s:Style('shWrapLineOperator', s:lightblue, '', '')
call s:Style('shShellVariables', s:lightblue, '', '')

call s:Style('yamlBlockMappingKey', s:lightblue, '', '')
call s:Style('yamlKeyValueDelimiter', s:bright_red, '', '')
call s:Style('yamlBlockCollectionItemStart', '', '', '')

call s:Style('yamlBlockScalarHeader', s:lightblue, '', '')
call s:Style('yamlBlockSequenceStart', s:lightblue, '', '')
call s:Style('yamlTSString', s:yamlstring, '', '')
call s:Style('yamlString', s:yamlstring, '', '')

call s:Style('luaFunc', s:lightblue, '', 'BOLD')
call s:Style('luaIn', '', '', '')
call s:Style('luaFunction', s:lightblue, '', 'BOLD')
call s:Style('luaStatement', '', '', '')
call s:Style('luaRepeat', '', '', '')
call s:Style('luaCondStart', '', '', '')
call s:Style('luaTable', '', '', '')
call s:Style('luaConstant', '', '', '')
call s:Style('luaElse', '', '', '')
call s:Style('luaCondElseif', '', '', '')
call s:Style('luaCond', '', '', '')
call s:Style('luaCondEnd', '', '', '')

call s:Style('pythonStatement', s:lightblue, '', 'BOLD')
call s:Style('pythonLambdaExpr', s:lightblue, '', '')
call s:Style('pythonInclude', s:lightblue, '', '')
call s:Style('pythonFunction', s:lightblue, '', '')
call s:Style('pythonClass', s:lightblue, '', '')
call s:Style('pythonParameters', '', '', '')
call s:Style('pythonParam', '', '', '')
call s:Style('pythonBrackets', '', '', '')
call s:Style('pythonClassParameters', '', '', '')
call s:Style('pythonSelf', s:lightblue, '', '')

call s:Style('pythonConditional', s:lightblue, '', '')
call s:Style('pythonRepeat', '', '', '')
call s:Style('pythonException', s:lightblue, '', '')
call s:Style('pythonOperator', '', '', '')
call s:Style('pythonExtraOperator', '', '', '')
call s:Style('pythonExtraPseudoOperator', '', '', '')

call s:Style('pythonDecorator', '', '', '')
call s:Style('pythonDottedName', '', '', '')
call s:Style('pythonComment', s:grey, '', '')
call s:Style('pythonCoding', '', '', '')
call s:Style('pythonRun', '', '', '')
call s:Style('pythonTodo', s:grey, s:search_gold, 'BOLD')

call s:Style('pythonError', '', '', '')
call s:Style('pythonIndentError', '', '', '')
call s:Style('pythonSpaceError', '', '', '')

call s:Style('pythonString', s:grey, '', '')
call s:Style('pythonDocstring', s:grey, '', '')
call s:Style('pythonUniString', s:grey, '', '')
call s:Style('pythonRawString', s:grey, '', '')
call s:Style('pythonUniRawString', s:grey, '', '')

call s:Style('pythonEscape', '', '', '')
call s:Style('pythonEscapeError', '', '', '')
call s:Style('pythonUniEscape', '', '', '')
call s:Style('pythonUniEscapeError', '', '', '')
call s:Style('pythonUniRawEscape', '', '', '')
call s:Style('pythonUniRawEscapeError', '', '', '')

call s:Style('pythonStrFormatting', '', '', '')
call s:Style('pythonStrFormat', '', '', '')
call s:Style('pythonStrTemplate', '', '', '')
call s:Style('pythonDocTest', '', '', '')
call s:Style('pythonDocTest2', '', '', '')

call s:Style('pythonNumber', s:bright_red, '', '')
call s:Style('pythonHexNumber', s:bright_red, '', '')
call s:Style('pythonOctNumber', s:bright_red, '', '')
call s:Style('pythonBinNumber', s:bright_red, '', '')
call s:Style('pythonFloat', s:bright_red, '', '')
call s:Style('pythonOctError', '', '', '')
call s:Style('pythonHexError', '', '', '')
call s:Style('pythonBinError', '', '', '')

call s:Style('pythonBuiltinType', s:lightblue, '', '')
call s:Style('pythonBuiltinObj', '', '', '')
call s:Style('pythonBuiltinFunc', '', '', '')

call s:Style('pythonExClass', '', '', '')

call s:Style('pyrexStatement', '', '', '')
call s:Style('pyrexType', '', '', '')
call s:Style('pyrexStructure', '', '', '')
call s:Style('pyrexInclude', '', '', '')
call s:Style('pyrexAccess', '', '', '')
call s:Style('pyrexBuiltin', '', '', '')
call s:Style('pyrexForFrom', '', '', '')
call s:Style('pythonImport', '', '', '')
call s:Style('pythonExceptions', '', '', '')
call s:Style('pythonPreCondit', '', '', '')
call s:Style('pythonBuiltin', s:lightblue, '', '')
call s:Style('pythonBoolean', '', '', '')
call s:Style('pythonBytesEscape', '', '', '')


call s:Style('vimCommand', '', '', '')
call s:Style('vimVar', '', '', '')
call s:Style('vimFuncKey', '', '', '')
call s:Style('vimFunction', '', '', '')
call s:Style('vimNotFunc', '', '', '')
call s:Style('vimMap', '', '', '')
call s:Style('vimAutoEvent', '', '', '')
call s:Style('vimMapModKey', '', '', '')
call s:Style('vimFuncName', '', '', '')
call s:Style('vimIsCommand', '', '', '')
call s:Style('vimFuncVar', '', '', '')
call s:Style('vimLet', '', '', '')
call s:Style('vimContinue', '', '', '')
call s:Style('vimMapRhsExtend', '', '', '')
call s:Style('vimCommentTitle', '', '', '')
call s:Style('vimBracket', '', '', '')
call s:Style('vimParenSep', '', '', '')
call s:Style('vimNotation', '', '', '')
call s:Style('vimOper', '', '', '')
call s:Style('vimOperParen', '', '', '')
call s:Style('vimSynType', '', '', '')
call s:Style('vimSynReg', '', '', '')
call s:Style('vimSynRegion', '', '', '')
call s:Style('vimSynMtchGrp', '', '', '')
call s:Style('vimSynNextgroup', '', '', '')
call s:Style('vimSynKeyRegion', '', '', '')
call s:Style('vimSynRegOpt', '', '', '')
call s:Style('vimSynMtchOpt', '', '', '')
call s:Style('vimSynContains', '', '', '')
call s:Style('vimGroupName', '', '', '')
call s:Style('vimGroupList', '', '', '')
call s:Style('vimHiGroup', '', '', '')
call s:Style('vimGroup', '', '', '')
call s:Style('vimOnlyOption', '', '', '')

call s:Style('makeIdent', '', '', '')
call s:Style('makeSpecTarget', '', '', '')
call s:Style('makeTarget', '', '', '')
call s:Style('makeStatement', '', '', '')
call s:Style('makeCommands', '', '', '')
call s:Style('makeSpecial', '', '', '')

call s:Style('cmakeStatement', '', '', '')
call s:Style('cmakeArguments', '', '', '')
call s:Style('cmakeVariableValue', '', '', '')

call s:Style('cmakeCommand', '', '', '')
call s:Style('cmakeCommandConditional', '', '', '')
call s:Style('cmakeKWset', '', '', '')
call s:Style('cmakeKWvariable_watch', '', '', '')
call s:Style('cmakeKWif', '', '', '')
call s:Style('cmakeArguments', '', '', '')
call s:Style('cmakeKWproject', '', '', '')
call s:Style('cmakeGeneratorExpressions', '', '', '')
call s:Style('cmakeGeneratorExpression', '', '', '')
call s:Style('cmakeVariable', '', '', '')
call s:Style('cmakeProperty', '', '', '')
call s:Style('cmakeKWforeach', '', '', '')
call s:Style('cmakeKWunset', '', '', '')
call s:Style('cmakeKWmacro', '', '', '')
call s:Style('cmakeKWget_property', '', '', '')
call s:Style('cmakeKWset_tests_properties', '', '', '')
call s:Style('cmakeKWmessage', '', '', '')
call s:Style('cmakeKWinstall_targets', '', '', '')
call s:Style('cmakeKWsource_group', '', '', '')
call s:Style('cmakeKWfind_package', '', '', '')
call s:Style('cmakeKWstring', s:string_grey, '', '')
call s:Style('cmakeKWinstall', '', '', '')
call s:Style('cmakeKWtarget_sources', '', '', '')

call s:Style('bashStatement', s:lightblue, '', '')
call s:Style('shDerefVar', s:lightblue, '', '')
call s:Style('shDerefSimple', s:lightblue, '', '')
call s:Style('shFunction', s:lightblue, '', '')
call s:Style('shStatement', s:lightblue, '', '')
call s:Style('shLoop', s:lightblue, '', '')
call s:Style('shQuote', s:grey, '', '')
call s:Style('shCaseEsac', s:lightblue, '', '')
call s:Style('shSnglCase', s:lightblue, '', '')
call s:Style('shFunctionOne', s:lightblue, '', '')
call s:Style('shCase', s:lightblue, '', '')
call s:Style('shSetList', s:lightblue, '', '')

call s:Style('DiffAdd',        s:black,   s:bright_green,     '')
call s:Style('DiffDelete',     s:black,   s:bright_red,       '')
call s:Style('DiffChange',     s:black,   s:white,   'bold')
call s:Style('DiffText',       s:black,   s:white,   'bold')


call s:Style('SpellBad', '', '', 'UNDERCURL')
call s:Style('SpellCap', '', '', 'UNDERCURL')
call s:Style('SpellRare', '', '', 'UNDERCURL')
call s:Style('SpellLocal', '', '', '')

call s:Style('gitcommitSummary', '', '', '')
call s:Style('gitcommitHeader', '', '', '')
call s:Style('gitcommitSelectedType', '', '', '')
call s:Style('gitcommitSelectedFile', '', '', '')
call s:Style('gitcommitUntrackedFile', '', '', '')
call s:Style('gitcommitBranch', '', '', '')
call s:Style('gitcommitDiscardedType', '', '', '')
call s:Style('gitcommitDiff', '', '', '')

call s:Style('diffFile', '', '', '')
call s:Style('diffSubname', '', '', '')
call s:Style('diffIndexLine', '', '', '')
call s:Style('diffAdded', '', '', '')
call s:Style('diffRemoved', '', '', '')
call s:Style('diffLine', '', '', '')
call s:Style('diffBDiffer', '', '', '')
call s:Style('diffNewFile', '', '', '')

call s:Style('xmlProcessingDelim', '', '', '')
call s:Style('xmlString', s:grey, '', '')
call s:Style('xmlEqual', '', '', '')
call s:Style('xmlAttrib', '', '', '')
call s:Style('xmlAttribPunct', '', '', '')
call s:Style('xmlTag', '', '', '')
call s:Style('xmlTagName', '', '', '')
call s:Style('xmlEndTag', '', '', '')
call s:Style('xmlNamespace', '', '', '')

call s:Style('sedST', '', '', '')
call s:Style('sedFlag', '', '', '')
call s:Style('sedRegexp47', '', '', '')
call s:Style('sedRegexpMeta', '', '', '')
call s:Style('sedReplacement47', '', '', '')
call s:Style('sedReplaceMeta', '', '', '')
call s:Style('sedAddress', '', '', '')
call s:Style('sedFunction', '', '', '')
call s:Style('sedBranch', '', '', '')
call s:Style('sedLabel', '', '', '')

call s:Style('awkPatterns', s:grey, '', '')
call s:Style('awkSearch', '', '', '')
call s:Style('awkRegExp', '', '', '')
call s:Style('awkCharClass', '', '', '')
call s:Style('awkFieldVars', '', '', '')
call s:Style('awkStatement', '', '', '')
call s:Style('awkFunction', '', '', '')
call s:Style('awkVariables', '', '', '')
call s:Style('awkArrayElement', '', '', '')
call s:Style('awkOperator', '', '', '')
call s:Style('awkBoolLogic', '', '', '')
call s:Style('awkExpression', '', '', '')
call s:Style('awkSpecialPrintf', '', '', '')

call s:Style('TSFunction', s:lightblue, '', '')
call s:Style('TSFuncBuiltin', s:lightblue, '', '')
call s:Style('TSComment', s:grey, '', '')
call s:Style('TSString', s:grey, '', '')


call s:Style('NvimTreeFolderIcon', '', '', '')
call s:Style('NvimTreeGitDirty', s:bright_red, '', '')
call s:Style('NvimTreeGitStaged', s:lightblue, '', '')
call s:Style('NvimTreeGitMerge', s:lightblue, '', '')
call s:Style('NvimTreeGitRenamed', s:lightblue, '', '')
call s:Style('NvimTreeGitNew', s:green, '', '')
call s:Style('NvimTreeGitDeleted', s:grey, '', '')
call s:Style('NvimTreeDirty', s:bright_red, '', '')
call s:Style('NvimTreeStaged', s:lightblue, '', '')
call s:Style('NvimTreeMerge', s:lightblue, '', '')
call s:Style('NvimTreeRenamed', s:lightblue, '', '')
call s:Style('NvimTreeNew', s:red, '', '')
call s:Style('NvimTreeDeleted', s:lightblue, '', '')
call s:Style('NvimTreeSpecialFile', s:bright_red, '', '')
call s:Style('NvimTreeImageFile', s:bright_red, '', '')
call s:Style('NvimTreeRootFolder', s:lightblue, '', '')



" Terminal Colors {{{

let g:terminal_ansi_colors = [ s:black, s:red, s:white, s:grey, s:light_grey, s:menu_grey, s:search_grey, s:string_grey, s:context_grey, s:bright_red, s:lightblue, s:search_gold, s:green, s:bright_green ]

let g:terminal_color_0 = s:black
let g:terminal_color_1 = s:red
let g:terminal_color_10 = s:red
let g:terminal_color_15 = s:white
let g:terminal_color_242 = s:grey
let g:terminal_color_236 = s:light_grey
let g:terminal_color_248 = s:menu_grey
let g:terminal_color_236 = s:search_grey
let g:terminal_color_244 = s:string_grey
let g:terminal_color_238 = s:context_grey
let g:terminal_color_197 = s:bright_red
let g:terminal_color_67 = s:lightblue
let g:terminal_color_220 = s:search_gold
let g:terminal_color_2 = s:green
let g:terminal_color_112 = s:bright_green


" }}}
